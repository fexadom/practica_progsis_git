# Pr�ctica 5: Uso de git para gestionar un proyecto de software #

Este es un repositorio esqueleto para la pr�ctica 5 de la materia Programaci�n de Sistemas (CCPG1008) P1 de la ESPOL.
Practica introductoria al uso de Git como herramienta de gesti�n de proyectos. 

### �C�mo empiezo? ###

* Hacer un fork de este repositorio a su cuenta personal de Bitbucket (una cuenta por grupo)
* Clonar el repositorio en su cuenta (no este) en su computadora del laboratorio
* Completar la pr�ctica en grupo
* Haga commit y push a su trabajo
* El entregable es un enlace al repositorio

### Integrantes ###

Edite esta lista y a�ada los nombres de los integrantes, luego borre esta l�nea.

* Integrante 1
* Integrante 2
